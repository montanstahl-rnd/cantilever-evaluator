# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import pandas as pd
import numpy as np
import csv
import xlrd
import openpyxl

from point import Point
from result import Result

def calculate_required_levels(actual_pos):
    required_levels_dict = {}
    actual_pos_grouped = actual_pos.groupby('Codice articolo')
    for name, group in actual_pos_grouped:
        levels = group['Lane - Cantilever - Level'].nunique()
        required_levels_dict[name] = levels

    sum = 0
    for item1, v in required_levels_dict.items():
        sum += v

    print('Levles to cover: ', sum)

    return required_levels_dict

# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    # Read actual positions
    xl_file = pd.ExcelFile("data\in\Articoli_per_ubicazione.xlsx")
    dfs_items_current_state = {sheet_name: xl_file.parse(sheet_name)
           for sheet_name in xl_file.sheet_names}

    # Read withdrawal frequency
    w_freq = dfs_items_current_state['Withdrawal frequencies']

    # Read actual positions
    actual_pos = dfs_items_current_state['Actual positions']

    # Read level eval
    level_eval = pd.read_csv("/data/level_eval.csv")
    level_eval = level_eval.sort_values(by='Cost')

    # Read rotation index

    # Calculate how many levels are required for each item
    print('Calculation of required levels in progress...')
    required_levels_dict = calculate_required_levels(actual_pos)



    # Calculate the ABC matrix

    # Divide the levels in five areas

    # Assign items with first fit

    item_to_allocate = w_freq[w_freq['Frequenza Cumulata %'] < 80]

    for index, row in item_to_allocate.iterrows():
        print(row)
