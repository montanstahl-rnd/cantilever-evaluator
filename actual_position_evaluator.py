# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import pandas as pd
import numpy as np
import csv
import xlrd
import openpyxl

from point import Point
from result import Result

# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    # Read level evaluations
    level_eval = pd.read_csv("/data/results.csv")

    # Read Excel file
    xl_file = pd.ExcelFile("data/Articoli_per_ubicazione.xlsx")
    dfs = {sheet_name: xl_file.parse(sheet_name)
           for sheet_name in xl_file.sheet_names}

    # Read withdrawal frequency
    w_freq = dfs['Withdrawal frequencies']

    # Read actual positions
    actual_pos = dfs['Actual positions']

    # Calculate the current average time
    total_frequency = 0
    total_cost = 0

    for index, row in w_freq.iterrows():
        item_code = row['Codice articolo']
        frequency = row['Frequenza']

        if actual_pos[actual_pos['Codice articolo'] == item_code].empty:
            continue
        levels = actual_pos[actual_pos['Codice articolo'] == item_code]["Lane - Cantilever - Level"]

        if level_eval[level_eval['Lane-Cantilever-Level'].isin(levels)].empty:
            continue
        level_evals = level_eval[level_eval['Lane-Cantilever-Level'].isin(levels)]

        avg_cost = np.average(level_evals['Cost'])
        total_cost += avg_cost * frequency
        total_frequency += frequency

    print('Total average cost: ' + str(total_cost/total_frequency))

