# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import pandas as pd
import numpy as np
import csv
import xlrd
import openpyxl

from point import Point
from result import Result

# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    # Read Excel file
    xl_file = pd.ExcelFile("data\in\DDT_Rows.xlsx")
    dfs = {sheet_name: xl_file.parse(sheet_name)
           for sheet_name in xl_file.sheet_names}

    # Read withdrawal frequency
    ddt_rows = dfs['DDT Rows']

    grouped = ddt_rows.groupby('Numero documento')

    D = {}

    for name, group in grouped:
        item_codes = group['Codice articolo']
        for i1, v1 in item_codes.items():
            for i2, v2 in item_codes.items():
                if v1 != v2:
                    if v1 not in D:
                        D[v1] = {}
                    if v2 not in D[v1]:
                        D[v1][v2] = 1
                    else:
                        D[v1][v2] += 1

    CD = {}
    for item1, v in D.items():
        for item2, d in v.items():

            if item1 not in CD:
                CD[item1] = {}
            if item2 not in CD:
                CD[item2] = {}

            if item1 not in CD[item2] and item2 not in CD[item1]:
                CD[item1][item2] = d

    # Write the result file
    with open('data\out\pair_eval.csv', 'w', newline='') as csv_file:
        wr = csv.writer(csv_file, delimiter=',')

        wr.writerow(["item1", "item2", "frequency"])

        for item1, v in CD.items():
            for item2, d in v.items():
                wr.writerow([str(item1), str(item2), str(d)])

    # D = {}
    # for city1, cords1 in cords.items():
    #     D[city1] = {}
    #     for city2, cords2 in cords.items():
    #         D[city1][city2] = dist(cords1, cords2)
    # for city1, v in D.items():
    #     for city2, d in v.items():
    #         print city1, city2, d