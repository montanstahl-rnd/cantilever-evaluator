# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import pandas as pd
import numpy as np
import csv
import xlrd
import openpyxl

from point import Point
from result import Result

# Manhattan distance  | a−c | + | b−d |
def calculate_manhattan_distance(p1,p2):
    return np.abs(p1.x-p2.x) + np.abs(p1.y-p2.y)

# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    # Read Excel file
    xl_file = pd.ExcelFile("data\in\Coordinate_mag_8S.xlsx")
    dfs = {sheet_name: xl_file.parse(sheet_name)
           for sheet_name in xl_file.sheet_names}

    # Read coordinates
    c_df = dfs['Coordinates']

    # Read parameters
    p_df = dfs['Parameters']
    params = p_df.iloc[0]
    source_point = Point(params['Order area X'], params['Order area Y'])
    vl_e = params['VL_E']
    vl_f = params['VL_F']
    vh_e_up = params['VH_E_UP']
    vh_e_down = params['VH_E_DOWN']
    vh_f_up = params['VH_F_UP']
    vh_f_down = params['VH_F_DOWN']
    h_delta = params['H_DELTA']/1000
    tdc_e_easy = params['TDC_E_EASY']
    tdc_f_easy = params['TDC_F_EASY']
    tdc_e_hard = params['TDC_E_HARD']
    tdc_f_hard = params['TDC_F_HARD']

    results = []

    # Calculate the level evaluation
    for index, row in c_df.iterrows():
        # Calculate the Manhattan distances
        dest_point = Point(row['X'], row['Y'])
        man_distance = calculate_manhattan_distance(source_point, dest_point)

        # Calculate the weighted distance
        l_w = man_distance * row['DPF']/1000

        # Get height, basket collection time, basket depot time
        h = row['H']/1000
        t_bc = row['Basket collection time']
        t_bd = row['Basket depot time']
        dc_e_easy = row['Easy direction changes empty']
        dc_f_easy = row['Easy direction changes full']
        dc_e_hard = row['Hard direction changes empty']
        dc_f_hard = row['Hard direction changes full']

        # Calculate source to destination time (empty forklift)
        t1 = vl_e * l_w

        # Calculate the elevation time (empty forklift)
        t2 = vh_e_up * h

        # Basket collection time (can be omitted)
        t3 = t_bc

        # Calculate the lowering time (full forklift)
        t4 = vh_f_down * (np.abs(h-h_delta))

        # Calculate destination to source time (full forklift)
        t5 = vl_f * l_w

        # Calculate source to destination time (full forklift)
        t6 = vl_f * l_w

        # Calculate the elevation time (full forklift)
        t7 = vh_f_up * h

        # Basket depot time (can be omitted)
        t8 = t_bd

        # Calculate the lowering time (empty forklift)
        t9 = vh_e_down * (np.abs(h-h_delta))

        # Calculate destination to source time (full forklift)
        t10 = 0

        # Add penalties for direction changes
        t11 = dc_e_easy * tdc_e_easy + dc_f_easy * tdc_f_easy + dc_e_hard * tdc_e_hard + dc_f_hard * tdc_f_hard

        # Sum up all the timings
        total_time = t1+t2+t3+t4+t5+t6+t7+t8+t9+t10+t11

        results.append(Result(row['Lane - Cantilever - Level'], row['Lane'], row['Cantilever'], row['Level'], total_time))

    # Order the result by ranking (time)
    # print(results)

    # Write the result file
    with open('data\out\level_eval.csv', 'w', newline='') as csv_file:
        wr = csv.writer(csv_file, delimiter=',')

        wr.writerow(["Lane-Cantilever-Level", "Lane", "Cantilever", "Level", "Cost"])

        for res in results:
            wr.writerow([res.lane_cantilever_level, res.lane, res.cantilever, res.level, res.total_time])


